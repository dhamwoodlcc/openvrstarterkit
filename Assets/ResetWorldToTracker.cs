﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ResetWorldToTracker : MonoBehaviour {

	public Transform world;
	public Transform trackerMatchPoint;

	// Use this for initialization
	void OnEnable () {

		newPosesAction.enabled = true;

	}
	
	// Update is called once per frame
	void Update () {
		/*if (Input.GetKeyUp (KeyCode.Space)) {
			transform.Translate (tracker.position - trackerMatchPoint.position);
		}*/

	}

	IEnumerator WaitFrameThenTranslate()
	{
		yield return new WaitForEndOfFrame();
		world.Translate (transform.position - trackerMatchPoint.position);
	}

	private void FirstPoseAction(TrackedDevicePose_t[] poses)
	{
		StartCoroutine (WaitFrameThenTranslate());
		newPosesAction.enabled = false;

	}

	SteamVR_Events.Action newPosesAction;

	ResetWorldToTracker()
	{
		newPosesAction = SteamVR_Events.NewPosesAction(FirstPoseAction);
	}


}
